require "time"

def measure(time = 1, &prc)
   my_time = Time.now
   fake_time = time.times { prc.call }
  (Time.now - my_time) /time
end
