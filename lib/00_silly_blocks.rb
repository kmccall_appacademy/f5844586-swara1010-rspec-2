def reverser(string)
  string_splt = string.split
  string_arr = []
  string_splt.each do |word|
    string_arr << word.reverse
  end
  string_arr.join(" ")
end

def adder(value=1, &prc)
  prc.call + value
end

def repeater(number=1, &prc)
  number.times {prc.call}
end
